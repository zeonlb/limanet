import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  ImageBackground,
  Image
} from "react-native";
import Spinner from "react-native-spinkit";

// const defaultBackground = require('../img/bgimage.jpg');
const logo = require('../img/LimaNET.png');
const SCREEN_HEIGHT = Dimensions.get('window').height;
const SCREEN_WIDTH = Dimensions.get('window').width;

export default function LoadingScreen(props) {
  return (
    <View style={styles.container}>
      {/* <ImageBackground source={defaultBackground} style={styles.ImageBackground}> */}
        <Image source={logo} style={styles.Image}/>
        <Spinner style={styles.spinner} isVisible={true} size={SCREEN_WIDTH/6} type={'Circle'} color={"#0a7dfd"}/>
      {/* </ImageBackground> */}
    </View>
    
);
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  Image:{
    width: SCREEN_WIDTH - 100,
    height: (SCREEN_WIDTH - 100) / 4.371875,
    resizeMode: 'stretch',
    justifyContent: "center",
    alignItems: "center",
  },
  ImageBackground:{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: '100%',
    height: '100%',
},
spinner: {
  marginTop: 80
},
});