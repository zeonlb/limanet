import React, { useEffect, useState  } from 'react';
import messaging from '@react-native-firebase/messaging';
import LinearGradient from 'react-native-linear-gradient';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { 
    Text, 
    View, 
    StyleSheet, 
    Button, 
    StatusBar,
    TouchableOpacity,
    Image, 
    SafeAreaView,
    Platform,
    Dimensions,
    NativeModules,
    ScrollView } from "react-native";
// import {AuthContext} from '../../../App';
import RNBootSplash from "react-native-bootsplash";
// import styles from "./Logo.style";
import { useHeaderHeight } from '@react-navigation/stack';

const { StatusBarManager } = NativeModules;
const HeaderHeight = ( StatusBarManager.HEIGHT * 2 ) + StatusBarManager.HEIGHT;
const HeaderImgHeight = HeaderHeight - (StatusBarManager.HEIGHT + 15);
const { width, height } = Dimensions.get('window');


export default function Home(props) {

    const headerHeight = useHeaderHeight();    

  // const {navigation, uid, gid, deposit, credit, fio, address, activated, last_connection, local_mac, real_ip, houseid } = props;
  const { navigation, route } = props;
  const { uid, gid, deposit, credit, fio, address, activated, last_connection, local_mac, real_ip, houseid } = route.params;
  const [tokenid, setTokenid] = useState('');
  const [slider1ActiveSlide, setslider1ActiveSlide] = useState(1);
  const [ref, setRef] = useState('');

  const logo_png = require('../../img/logo.png');
  const stats = require('../../img/stats.png');
  const tariff = require('../../img/tariff.png');
  const cart = require('../../img/cart.png');
  const promotion = require('../../img/promotion.png');
  const notif = require('../../img/notif.png');
  const settings = require('../../img/settings.png');
  
  useEffect(() => {
    RNBootSplash.hide();
  }, []);
  
  useEffect(() => {
    // Get the device token
    messaging()
      .getToken()
      .then(token => {
        return setTokenid(token);
      });
      console.log('+++++++++++++++++++++++++++++++++++++++++++++');
    // Listen to whether the token changes
    return messaging().onTokenRefresh(token => {
      // saveTokenToDatabase(token);
      console.log('-----------------------------------------------');
      setTokenid(token);
    });
  }, []);


    const ENTRIES1 = [
        {
            title: 'Швидкісний домашній Інтернет',
            subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
            illustration: 'https://lima.net.ua/images/pagebuilder/backgrounds/gigapard_UA.png'
        },
        {
            title: 'Швидкісний домашній Інтернет',
            subtitle: 'Lorem ipsum dolor sit amet',
            illustration: 'https://lima.net.ua/images/telegrammbot/1080x399ukr.jpg'
        },
        {
            title: 'Швидкісний домашній Інтернет',
            subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
            illustration: 'https://lima.net.ua/images/pagebuilder/backgrounds/viber_ukr.png'
        },
        {
            title: 'Швидкісний домашній Інтернет',
            subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
            illustration: 'https://lima.net.ua/images/pagebuilder/backgrounds/bckg1976.jpg'
        }
    ];
    const itemWidth = Math.round((75 * width) / 100) + (Math.round((2 * width) / 100) * 2);
    
    const myrenderItem = ({item, index}) => {
        return (
            <View style={{ width: width, height: width / 1.66, }}>
                <View style={styles.imageContainer}>
                    <Image  source={{ uri: item.illustration }}  style={styles.image}/>
                    <Text style={styles.title}>{ item.title }</Text>
                    <Text style={styles.title}>{ item.subtitle }</Text>   
                </View>
            </View>
        );
    };
    
    
// console.log(headerHeight);


//   const { signOut } = React.useContext(AuthContext);
  return (
    <SafeAreaView style={styles.container}>
        <StatusBar barStyle = "light-content" backgroundColor={'transparent'} hidden = {false} translucent = {true}/>
        {/* <View style={styles.header}>
            <Image source={logo_png} style={styles.logoHeader}/>
            <Text style={styles.textHeader}>ВАШ ПРОВОДНИК В МИРЕ ИНТЕРНЕТ</Text>
        </View> */}
        <View style={styles.slider}>
            <LinearGradient
                colors={['#3E80FF', '#F5FCFF']}
                startPoint={{ x: 1, y: 0 }}
                endPoint={{ x: 0, y: 1 }}
                style={styles.gradient}
            />
            <Carousel
                ref={c => setRef(c)}
                data={ENTRIES1}
                renderItem={myrenderItem}
                sliderWidth={width}
                itemWidth={width}
                // hasParallaxImages={false}
                firstItem={0}
                inactiveSlideScale={0.8}
                inactiveSlideOpacity={0.6}
                // inactiveSlideShift={20}
                // containerCustomStyle={styles.slideryyy}
                // contentContainerCustomStyle={styles.sliderContentContainer}
                // loop={true}
                // loopClonesPerSide={2}
                autoplay={false}
                // autoplayDelay={1000}
                // autoplayInterval={3000}
                onSnapToItem={(index) => setslider1ActiveSlide(index) }
            />
            {/* <Pagination
                dotsLength={ENTRIES1.length}
                activeDotIndex={slider1ActiveSlide}
                containerStyle={styles.paginationContainer}
                dotColor={'rgba(89, 120, 255, 0.92)'}
                dotStyle={styles.paginationDot}
                inactiveDotColor={'rgba(89, 120, 255, 0.42)'}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
                carouselRef={ref}
                tappableDots={!!ref}
            /> */}
            
        </View>
        <View style={styles.body}>
            <View style={styles.bodyTitle}>
                <Text style={styles.textBodyTitle}>Главное Меню</Text>
            </View>
            <View style={styles.bodyButtonArea}>
                <View style={styles.bodyButtonRow}>
                    <TouchableOpacity style={styles.bodyButton} onPress={() => navigation.navigate('Statistics')}>
                        <Image source={stats} style={styles.imgButton}/>
                        <Text style={styles.menuButtonText}>СТАТИСТИКА</Text>
                    </TouchableOpacity>
                    <View style={styles.hairlineVertical}/>
                    <TouchableOpacity style={styles.bodyButton} onPress={() => navigation.navigate('Payment')}>
                        <Image source={cart} style={styles.imgButton}/>
                        <Text style={styles.menuButtonText}>ПОПОЛНЕНИЕ</Text>
                    </TouchableOpacity>
                    <View style={styles.hairlineVertical}/>
                    <TouchableOpacity style={styles.bodyButton} onPress={() => navigation.navigate('News')}>
                        <Image source={notif} style={styles.imgButton}/>
                        <Text style={styles.menuButtonText}>СООБЩЕНИЯ</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.hairlineHorizont}/>
                <View style={styles.bodyButtonRow}>
                    <TouchableOpacity style={styles.bodyButton} onPress={() => navigation.navigate('Promotion')}>
                        <Image source={promotion} style={styles.imgButton}/>
                        <Text style={styles.menuButtonText}>АКЦИЯ</Text>
                    </TouchableOpacity>
                    <View style={styles.hairlineVertical}/>
                    <TouchableOpacity style={styles.bodyButton} onPress={() => navigation.navigate('Tariffs')}>
                        <Image source={tariff} style={styles.imgButton}/>
                        <Text style={styles.menuButtonText}>ТАРИФЫ</Text>
                    </TouchableOpacity>
                    <View style={styles.hairlineVertical}/>
                    <TouchableOpacity style={styles.bodyButton} onPress={() => navigation.navigate('Settings')}>
                        <Image source={settings} style={styles.imgButton}/>
                        <Text style={styles.menuButtonText}>НАСТРОЙКИ</Text>
                    </TouchableOpacity>
                </View>
            </View>
            
            {/* <Button title="Статистика" color="orange"  onPress={() => navigation.navigate('Statistics')}/> */}
        </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#3E80FF",
    //   justifyContent: "center",
    //   alignItems: "center",
    //   backgroundColor: "#F5FCFF"
    },
    header:{
        // flex: 1,
        justifyContent: 'flex-end',
        alignItems: "center",
        // backgroundColor: "#3E80FF",
        height: HeaderHeight,

    },
    logoHeader:{
        // width: 186,
        height: HeaderImgHeight,
        resizeMode: 'contain',
        // marginBottom: 10,
    },
    textHeader:{
        fontSize: 10,
        color: 'white',
        marginBottom: 5,
    },
    slider:{
        flex: 2,
        // backgroundColor: "red"
    },
    body:{
        flex: 3,
        backgroundColor: "#F5FCFF"
    },
    bodyTitle:{
        flex: 1,
        justifyContent: 'center',
        alignItems: "center",
        // backgroundColor: "green"
    },
    bodyButtonArea:{
        flex: 5,
        margin: 10,
    },
    bodyButtonRow:{
        flex: 1,
        flexDirection: 'row',
        // backgroundColor: "yellow"
    },
    bodyButton:{
        flex: 1,
        margin: 5,
        alignItems: "center",
        justifyContent: 'center',
        // borderColor: '#BAC5E9',
        // borderRightWidth: 2,
        // borderBottomWidth: 2,
        // borderRadius: 6,
        // backgroundColor: "pink"
    },
    textBodyTitle:{
        fontSize: 22,
        color: '#2C3550'
    },
    hairlineVertical: {
        backgroundColor: '#BAC5E9',
        width: 1
      },
    hairlineHorizont: {
        backgroundColor: '#BAC5E9',
        height: 1,
    },
    imgButton:{
        width: 60,
        height: 60,
        resizeMode: 'contain',
    },
    menuButtonText:{
        fontSize: 14,
        color: '#2C3550',
        marginTop: 10
        
    },
    gradient:{
        ...StyleSheet.absoluteFillObject,
    },
    slideryyy:{
        marginTop: 15,
        overflow: 'visible' // for custom animations
    },
    sliderContentContainer: {
        paddingVertical: 10 // for custom animation
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'cover',
        borderRadius: 0,
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0
    },
    imageContainer: {
        flex: 1,
        // marginBottom: -1, // Prevent a random Android rendering issue
        backgroundColor: 'white',
        borderTopLeftRadius: 0,
        borderTopRightRadius: 0
    },
    paginationContainer: {
        paddingVertical: 0
    },
    paginationDot: {
        width: 22,
        height: 8,
        borderRadius: 3,
        marginHorizontal: 0
    },
    slide:{
        backgroundColor: 'red'
    },
  });