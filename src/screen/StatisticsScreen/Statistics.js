import React, { useEffect, useState  } from 'react';
import { Text, View, Button, ScrollView, Image, ImageBackground } from "react-native";
import styles from "./Statistics.style";

export default function Statistics(props) {

  const { navigation, route } = props;
  const {
    uid,
    gid,
    deposit,
    credit,
    fio,
    blocked,
    activated,
    summa,
    local_mac,
    real_ip,
    bughtypeid,
    packet,
    fixed_cost,
    speed,
    real_price 
  } = route.params;

  const HeaderBackground = require('../../img/head2.png');
  const logo = require('../../img/user.png');
  const cardFlag = require('../../img/statistics/flag.png');
  const cardIconMoney = require('../../img/statistics/money.png');
  const cardIconTime = require('../../img/statistics/time.png');
  const cardIconTariff = require('../../img/statistics/tariff.png');
  const cardIconSpeed = require('../../img/statistics/speed.png');
  const textSpeed = speed ? Math.trunc(speed) : 'н/а';
  const balance = deposit ? Math.trunc(deposit) : 'н/а';
  const credit_round = credit ? Math.trunc(credit) : 'н/а';
  const textValidityPeriod = bughtypeid == 1 ? 'Осталось мес.:' : 'Осталось дней:';
  const texttValPeriod = bughtypeid == 1 ? ' мес.' : ' д.';
  const priceTarif = fixed_cost ? Math.trunc(fixed_cost) : 'н/а';
  const totalSumm = real_ip == 1 ? (Number(summa) + Number(real_price)) : Number(summa);
  const period = Math.trunc((Number(credit) + Number(deposit)) / totalSumm);
  const statusRealIp = (real_ip == 1) ? 'Реальный IP Активен' : 'Реальный IP Неактивен';
  const summRealIp = (real_ip == 1 && bughtypeid !== 1) ? ((parseInt(real_price * 100)) / 100) : (real_ip == 1 && bughtypeid == 1) ? Math.trunc(real_price) : '0.00';
  const cvet = (period > 3) ? '#25beb1' : (period < 1) ? '#f55b5b' : '#f9b256';
  const tval = bughtypeid == 1 ? 'грн/мес' : 'грн/день';
  const Status = blocked == 1 ? 
    <Text 
      style={{
        fontSize: 14, 
        backgroundColor: '#f55b5b',
        borderRadius: 10,
        paddingHorizontal: 6,
        paddingVertical: 2,  
        color: '#fff',
      }}>
      Отключен
    </Text>
    :
    <Text 
      style={{
        fontSize: 14, 
        backgroundColor: '#25beb1',
        borderRadius: 10,
        paddingHorizontal: 6,
        paddingVertical: 2, 
        color: '#fff',}}>
      Активен
    </Text>;
      
// console.log(styles.textCardPeriod)
  return (
    <View style={styles.container}>      
      <ImageBackground source={HeaderBackground} style={styles.HeaderImageBackground}>
        <View style={styles.HeaderBox}>
          <Image source={logo} style={styles.HeaderLogo}/>
          <Text style={styles.textHeaderOn}>номер моего договора</Text>
          <Text style={styles.textHeadUid}>uid:{uid}</Text>
          <Text style={styles.textHeadFio}>{fio}</Text>
        </View>
      </ImageBackground>
      <ScrollView>
        <View style={styles.card}>
          <View style={styles.v1}>
            <View style={styles.cardTitle}>
              <View style={styles.cardTitleLeft}>
                <Text style={styles.textCardTitle}>Финансы на счету</Text>
              </View>
              <View style={styles.cardTitleRight}>
                <Image  source={cardFlag}  style={styles.cardFlag}/>
              </View>
            </View>
            <View style={styles.cardBody}>
              <Image  source={cardIconMoney}  style={styles.cardIcon}/>
              <View style={styles.cardBodyText}>
                <View style={styles.textCardRow}>
                  <Text style={styles.textCard2}>Текущий баланс: </Text>
                  <Text style={styles.textCardSumm}>
                    {balance}
                    <Text style={styles.textUAH}> ₴</Text>
                  </Text>
                </View>
                <View style={styles.textCardRow}>
                  <Text style={styles.textCard3}>Предоставленный кредит:</Text>
                  <Text style={styles.textCardCred}>
                    {credit_round}
                    <Text style={styles.textUAH}> ₴</Text>
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.v1}>
            <View style={styles.cardTitle}>
              <View style={styles.cardTitleLeft}>
                <Text style={styles.textCardTitle}>Срок действия</Text>
              </View>
              <View style={styles.cardTitleRight}>
                <Image  source={cardFlag}  style={styles.cardFlag}/>
              </View>
            </View>
            <View style={styles.cardBody}>
              <Image  source={cardIconTime}  style={styles.cardIcon}/>
              <View style={styles.cardBodyText}>
                <View style={styles.textCardRow}>
                  <Text style={styles.textCard2}>{textValidityPeriod}</Text>
                  <Text style={[styles.textCardPeriod, {backgroundColor: cvet}]}>
                    {period}
                    <Text style={styles.textUAH}>{texttValPeriod}</Text>
                  </Text>
                </View>
                <View style={styles.textCardRow}>
                  <Text style={styles.textCard3}>Статус подключения:</Text>
                  {Status}
                </View>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.v1}>
            <View style={styles.cardTitle}>
              <View style={styles.cardTitleLeft}>
                <Text style={styles.textCardTitle}>Подключенный тариф</Text>
              </View>
              <View style={styles.cardTitleRight}>
                <Image  source={cardFlag}  style={styles.cardFlag}/>
              </View>
            </View>
            <View style={styles.cardBody}>
              <Image  source={cardIconTariff}  style={styles.cardIcon}/>
              <View style={styles.cardBodyText}>
                <View style={styles.textCardRow}>
                  <Text style={styles.textCard2}>Тариф:</Text>
                  <Text style={styles.textCardTarif}>{packet}</Text>
                </View>
                <View style={styles.textCardRow}>
                  <Text style={styles.textCard3}>Абонплата по тарифу:</Text>
                  <Text style={styles.textpriceTarif}>{priceTarif}<Text style={{fontSize: 12}}>грн/мес</Text></Text>
                </View>
                <View style={styles.textCardRow}>
                  <Text style={styles.textCard3}>{statusRealIp}</Text>
                  
                  <Text style={{color: '#1f57b4',fontSize: 16}}>{summRealIp}<Text style={{fontSize: 12}}>{tval}</Text></Text>
                </View>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.v1}>
            <View style={styles.cardTitle}>
              <View style={styles.cardTitleLeft}>
                <Text style={styles.textCardTitle}>Скорость подключения</Text>
              </View>
              <View style={styles.cardTitleRight}>
                <Image  source={cardFlag}  style={styles.cardFlag}/>
              </View>
            </View>
            <View style={styles.cardBody}>
              <Image  source={cardIconSpeed}  style={styles.cardIcon}/>
              <View style={styles.cardBodyText}>
                <View style={styles.textCardRow}>
                  <Text style={styles.textCardSpeed}>Входящая скорость до:</Text>
                  <Text style={styles.textpriceTarif}>{textSpeed}<Text style={{fontSize: 12}}> Мб/с</Text></Text>
                </View>
                <View style={styles.textCardRow}>
                  <Text style={styles.textCardSpeed}>Исходящая скорость до:</Text>
                  <Text style={styles.textpriceTarif}>{textSpeed}<Text style={{fontSize: 12}}> Мб/с</Text></Text>
                </View>
                <View style={styles.textCardRow}>
                  <Text style={styles.textCard3}>Мой MAC-адрес:</Text>
                  <Text style={{fontSize: 16}}>{local_mac}</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
        
        
        <View style={styles.podpis}>
            <Text style={styles.textpodpis}>Информация предоставляется</Text>
            <Text style={styles.textpodpis}>на основании  заключенного договора</Text>
        </View>
        
        {/* <Button title="Go to Payment"  onPress={() => navigation.navigate('Payment', {name: 'Jane'})}/> */}
      </ScrollView>
    </View>
  );
};