import { StyleSheet, Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#d7e1ed",
    },
    HeaderImageBackground: {
        width: '100%',
        height: 220,
        resizeMode: 'contain',
        justifyContent: "flex-end",
    },
    HeaderBox: {
        alignItems: "center",
        justifyContent: "flex-end",
        marginBottom: 10,
    },
    HeaderLogo: {
        width: 60,
        height: 60,
        resizeMode: 'contain',
    },
    textHeaderOn:{
        marginTop: 5,
        fontSize: 11,
        color: '#FFF',
        fontWeight: '100',
    },
    textHeadUid: {
        marginTop: -5,
        fontSize: 28,
        fontWeight: 'bold',
        color: '#FFF',
    },
    textHeadFio: {
        fontSize: 16,
        fontWeight: '100',
        color: '#FFF',
    },
    card: {
        marginTop: 20,
        width: width,
        height: 120,
        paddingHorizontal: 20,
    },
    v1: {
        flex: 1,
        backgroundColor: '#FFF',
        borderRadius: 10,
        borderTopWidth: 3,
        // borderTopRightRadius: 0,
        borderTopLeftRadius: 0,
        borderColor: '#25beb1',
    },
    cardTitle: {
        flexDirection: 'row',
        width: '100%',
        // alignItems: "center",
        // justifyContent: 'center',
        height: 25,
        // backgroundColor: 'red'
    },
    cardTitleLeft: {
        width: '50%',
        // alignItems: "center",
        justifyContent: 'center',
        backgroundColor: '#25beb1',
    },
    cardTitleRight: {
        width: '50%',
        alignItems: 'flex-start',
        justifyContent: 'center',
        // backgroundColor: 'green',
    },
    cardFlag: {
        height: 25,
        width: 25,
        resizeMode: 'contain',
        alignSelf: 'flex-start',
        marginLeft: -1,
    },
    textCardTitle: {
        marginTop: -3,
        paddingLeft: 20,
        fontSize: 14,
        fontWeight: '100',
        color: '#fff',
    },
    cardBody: {
        flexDirection: 'row',
        alignItems: "center",
        flex: 1,
        paddingHorizontal: 20,
        paddingVertical: 10,
        // backgroundColor: 'red',
    },
    cardIcon: {
        width: 70,
        height: 70,
        marginLeft: 0,
        marginRight: 0,
    },
    cardBodyText: {
        flex: 1,
        height: '100%',
        
        marginRight: 0,
        // backgroundColor: '#f9b256',
    },
    
    textCardRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: "center",
        paddingLeft: 10,
        // backgroundColor: 'green',
        // marginVertical: 5,
    },
    textCard2: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#1f57b4',
    },
    textCardSumm:{
        fontSize: 20,
        fontWeight: 'bold',
        color: '#fff',
        backgroundColor: '#25beb1',
        borderRadius: 10,
        paddingHorizontal: 12,
        paddingVertical: 5,
    },
    textUAH: {
        // color: '#fff',
        // fontSize: 14,
        // fontWeight: '400',

    },
    textCard3: {
        fontSize: 14,
        color: '#414141',
    },
    textCardCred: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#fff',
        backgroundColor: '#f9b256',
        borderRadius: 10,
        paddingHorizontal: 6,
        paddingVertical: 2,
    },

    textStyle:{
        fontSize: 13,
        paddingHorizontal: 10,
        paddingVertical: 10,
        // marginTop: 15,
    },
    textCardPeriod: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#fff',
        borderRadius: 10,
        paddingHorizontal: 12,
        paddingVertical: 5,
    },
    textCardTarif: {
        fontSize: 14,
        fontWeight: '100',
        color: '#25beb1',
    },
    textpriceTarif: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#1f57b4',
    },
    textCardSpeed: {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#1f57b4',
    },
    podpis: {
        alignItems: "center",
        justifyContent: "center",
        marginTop: 20,
        marginBottom: 10
    },
    textpodpis: {
        fontSize: 12,
        fontWeight: '100',
        color: '#666666',
    }
    // textCardTitle: {
    //     fontSize: 20,
    //     fontWeight: 'bold',
    //     color: '#2e323b',
    // },
    
    
    
    
});