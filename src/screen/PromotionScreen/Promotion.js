import React, { useEffect, useState  } from "react";
import { Text, View, StyleSheet, ScrollView, FlatList, Linking, ImageBackground, Image } from "react-native";

// import styles from "./Logo.style";

export default function Promotion(props) {
  const { navigation, route } = props;
  const { uid } = route.params;
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState({'connect_db':'waiting'});
  const mtsIcon = require('../../img/support/mts.png');
  const kyivIcon = require('../../img/support/kyiv.png');
  const HeaderBackground = require('../../img/head2.png');
  useEffect(() => {
    fetch('http://79.142.194.115/messages/10990')
    // fetch('http://79.142.194.115/messages/' + uid)
    .then((response) => response.json())
    .then((json) => {
      setData(json)
    })
    .catch((error) => console.error(error))
    .finally(() => setLoading(false));   
  }, []);
// console.log(data);
//  <Text onPress={()=>{Linking.openURL('tel:0487051715');} }>Городской: 048 705-17-15</Text>
//  <Text onPress={()=>{Linking.openURL('tel:0674881751');} }>Киевстар: 067 488-17-51</Text>
//  <Text onPress={()=>{Linking.openURL('tel:0950170789');} }>МТС: 095 017-07-89</Text>
// <View style={styles.container}>
//          <Respons value={data} />
//        </View>
  
  return (
    <View style={styles.container}>
      {/* <ImageBackground source={HeaderBackground} style={styles.HeaderImageBackground}>
        <View style={styles.HeaderBox}>
          <Image  source={mtsIcon}  style={styles.cardIcon}/>
          <Image  source={kyivIcon}  style={styles.cardIcon}/>
          <Text style={styles.textHeaderOn}>номер моего договора</Text>
          <Text style={styles.textHeadUid}>uid</Text>
          <Text style={styles.textHeadFio}>fdgfgdfg</Text>
        </View>
      </ImageBackground> */}
      
      {isLoading ? 
        <Text style={styles.textHeaderOn}>Загрузка ...</Text>
       : 
        <FlatList
        inverted
        data={data}
        renderItem={({ item }) => <Item value={item} key={item.messageid} />}
        keyExtractor={item => item.messageid}
      />
      }
    </View>
  );
};

function Item({ value }) {
  
  const key1 = value.messageid + '1f';
  const key2 = value.messageid + '2f';
  const key3 = value.messageid + '3f';
  const key4 = value.messageid + '4f';
  const persona = value.user_fio !== null ? '' : 'Техподдержка';
  const position = value.user_fio !== null ? 'flex-end' : 'flex-start';
  const positionText = value.user_fio !== null ? 'right' : 'left';
  const LeftRadius = value.user_fio !== null ? 20 : 0;
  const RightRadius = value.user_fio !== null ? 0 : 20;
  const colorMessage = value.user_fio !== null ? '#167bc3' : '#11ad65';
  const dateMessage = value.date.slice(0,10);
  const timeMessage = value.date.slice(11);
  return (
    <View style={styles.item} key={key1}>
      <View style={[styles.itemHeader, {justifyContent: position}]} key={key2}>
        <Text style={styles.textPersona}>{persona}</Text>
        <Text style={styles.textDate}>{dateMessage}</Text>
      </View>
      <View key={key3} style={[styles.itemMessage, {alignSelf: position, borderTopLeftRadius: LeftRadius, borderTopRightRadius: RightRadius, backgroundColor: colorMessage,}]}>
        <Text style={[styles.textMessage, {textAlign: positionText}]}>{value.message}</Text>
        <Text style={[styles.textTime, {textAlign: positionText}]}>{timeMessage}</Text>
      </View>
    </View>
  );
};

function Respons({ value }) {
  if(value.connect_db === 'waiting'){
    return <Text style={styles.textStyle}>Ожидание ответа...</Text>;
  };
  if(value.connect_db === 'fail'){
    return <Text style={styles.textStyle}>Ошибка подключения</Text>;
  };
  if(value.connect_db === 'ok'){
    if(value.result === 'no'){
      return <Text style={styles.textStyle}>Нет истории сообщений</Text>;
    };
  };
  return <Text style={styles.textStyle}>Ожидание ответа...</Text>;
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "stretch",
      backgroundColor: "#F5FCFF"
    },
    itemHeader: {
      flexDirection: 'row',
      alignItems: "center",
    },
    itemMessage:{
      maxWidth: '75%',
      borderBottomLeftRadius: 20,
      borderBottomRightRadius: 20,
    },
    textStyle:{
        fontSize: 12,
        paddingHorizontal: 10,
    },
    textPersona:{
      fontSize: 16,
      fontWeight: 'bold',
      color: '#414141',
    },
    textDate:{
      fontSize: 12,
      fontWeight: '100',
      color: '#414141',
      paddingLeft: 10,
    },
    textTime:{
      fontSize: 10,
      fontWeight: '100',
      color: '#eee',
      paddingHorizontal: 20,
      paddingBottom: 5,
      paddingTop: 0,
    },
    textMessage:{
      color: '#fff',
      fontSize: 14,
      paddingHorizontal: 10,
      paddingVertical: 0,
    },
    item: {
      padding: 5,
      marginVertical: 0,
      marginHorizontal: 10,
    },
    HeaderImageBackground: {
      width: '100%',
      height: 220,
      resizeMode: 'contain',
      justifyContent: "flex-end",
  },
  });