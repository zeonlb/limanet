import React from "react";
import { Text, View, StyleSheet, Button } from "react-native";
import {AuthContext} from '../../../App';

// import styles from "./Logo.style";

export default function Settings(props) {
  const { signOut } = React.useContext(AuthContext);
  const { navigation, route } = props;
  
  // const {  } = route.params;
  console.log(route.params);
  return (
    <View style={styles.container}>
      <Text style={styles.textStyle}>SettingsScreen</Text>
      <Button title="Go to Payment"  onPress={() => navigation.navigate('Payment', {name: 'Jane'})}/>
      <Button title="Sign Out"  onPress={() => signOut()}/>
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#F5FCFF"
    },
    textStyle:{
        fontSize: 20,
        paddingHorizontal: 10,
        // marginTop: 15,
    },
  });