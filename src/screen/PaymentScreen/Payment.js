import React from "react";
import { Text, View, StyleSheet, Button } from "react-native";

// import styles from "./Logo.style";

export default function Payment(props) {

  const { navigation } = props;

  return (
    <View style={styles.container}>
      <Text style={styles.textStyle}>PaymentScreen</Text>
      <Button title="Go to Jane's profile"  onPress={() => navigation.navigate('News', {name: 'Jane'})}/>
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#F5FCFF"
    },
    textStyle:{
        fontSize: 20,
        paddingHorizontal: 10,
        // marginTop: 15,
    },
  });