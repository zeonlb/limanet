import React from "react";
import { Text, View, StyleSheet } from "react-native";

// import styles from "./Logo.style";

export default function Tariffs(props) {

  const {  } = props;

  return (
    <View style={styles.container}>
      <Text style={styles.textStyle}>TariffsScreen</Text>
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#F5FCFF"
    },
    textStyle:{
        fontSize: 20,
        paddingHorizontal: 10,
        // marginTop: 15,
    },
  });