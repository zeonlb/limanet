import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  KeyboardAvoidingView,
  TouchableOpacity,
  ImageBackground,
  Image,
  StatusBar,
  TextInput,
  Alert
} from "react-native";
import RNBootSplash from "react-native-bootsplash";
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {AuthContext} from '../../App';

const logo_png = require('../img/logo.png');

export default function LoginScreen(props) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    useEffect(() => {
        RNBootSplash.hide();
        // RNBootSplash.hide({ duration: 250 }); 
      }, []);
    const { signIn } = React.useContext(AuthContext);

    const submitLoginPassword = () => {
        if (username !== '' && password !== ''){
            let data = {'username': username, 'password': password};
            signIn(data);
        }else{
            Alert.alert('Внимание !','Поле Логин или Пароль незаполнены !')
        };
    };
    

    return (
        <View style={styles.container}>
            <StatusBar barStyle = "light-content" hidden = {false} backgroundColor = "#3E80FF" translucent = {true}/>
            <Image source={logo_png} style={styles.ImageBackground}/>
            <KeyboardAvoidingView style={styles.block} behavior="padding">
                <View style={styles.blockinput}>
                    <Text style={styles.text}>Логин</ Text> 
                    <View style={styles.blockinputline}>
                        <Icon name="user" size={20} style={styles.icon}/>
                        <TextInput
                            placeholder = "Пример: Leonid"
                            style = {styles.input}
                            autoCapitalize = {'none'}
                            placeholderTextColor={'#FFFFFF77'}
                            autoCompleteType = {'off'}
                            autoCorrect = {false}
                            importantForAutofill = {'no'}
                            keyboardType = {'visible-password'}
                            maxLength = {32}
                            underlineColorAndroid={'transparent'}
                            onChangeText = {textLogin => setUsername(textLogin)}
                            value = {username}
                        />
                    </View>
                </View>
                <View style={styles.blockinput}>
                    <Text style={styles.text}>Пароль</ Text>
                    <View style={styles.blockinputline}>
                        <Icon name="lock" size={20} style={styles.icon}/>
                        <TextInput
                            placeholder = "Пример: Leo245rt"
                            placeholderTextColor={'#FFFFFF77'}
                            style = {styles.input}
                            autoCapitalize = {'none'}
                            autoCompleteType = {'off'}
                            autoCorrect = {false}
                            importantForAutofill = {'no'}
                            keyboardType = {'visible-password'}
                            maxLength = {32}
                            onChangeText = {textPassword => setPassword(textPassword)}
                            value = {password}
                        />
                    </View> 
                </View>
                <TouchableOpacity style={styles.submitButton} onPress={submitLoginPassword}>
                    <Text style={styles.submitButtonText}>ВОЙТИ</Text>
                </TouchableOpacity>
                <Text style={styles.TextHelp}>Введите логин и пароль</Text>
                <Text style={styles.TextHelp}>указанный в Вашем договоре</Text>
            </KeyboardAvoidingView>
                
        </View>
        
    );
  }
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor: "#3E80FF"
    },
    ImageBackground:{
        marginBottom: 60,
        justifyContent: "center",
        alignItems: "center",
        width: 186,
        height: 44,
    },
    block:{
        paddingLeft: 40,
        paddingRight: 40,
        width: '100%',
    },
    blockinput:{
        marginBottom: 30,
    },
    blockinputline:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#F3F6FF',
        borderBottomWidth: 1,
    },
    icon:{
        color: '#BAC5E9',
    },
    input: {
        flex: 1,
        color: 'white',
        fontSize: 18,
        height: 45,
        paddingBottom: 0,
        paddingLeft: 15,
    },
    text:{
        fontSize: 16,
        color: '#FFF',
        // color: '#BAC5E9',
    },
    submitButton: {
        backgroundColor: "white",
        justifyContent: "center",
        marginVertical: 15,
        alignItems: "center",
        height: 50,
        borderRadius: 6
    },
    submitButtonText: {
        color: "#3E80FF",
        fontSize: 16,
        fontWeight: '300'
    },
    TextHelp:{
        fontSize: 12,
        fontWeight: '400',
        color: '#BAC5E9',
        alignSelf: "center"
    }
  });