import React, { useEffect, useReducer, createContext, useMemo } from 'react';
import { StatusBar, Alert, Image } from 'react-native';

import messaging from '@react-native-firebase/messaging';

import AsyncStorage from '@react-native-community/async-storage';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';


import LoadingScreen from './src/screen/loadingScreen';
import LoginScreen from './src/screen/loginScreen';
import Statistics from './src/screen/StatisticsScreen/Statistics';
import Payment from './src/screen/PaymentScreen/Payment';
import News from './src/screen/NewsScreen/News';
import Promotion from './src/screen/PromotionScreen/Promotion';
import Settings from './src/screen/SettingsScreen/Settings';
import Tariffs from './src/screen/TariffsScreen/Tariffs';
import Home from './src/screen/HomeScreen/Home';

const Stack = createStackNavigator();
export const AuthContext = createContext();

function LogoTitle() {
  return (
    <Image style={{ width: 180, height: 45, resizeMode: 'contain' }} source={require('./src/img/logo.png')}
    />
  );
}


export default function App() {
  const [state, dispatch] = useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'READ_STORAGE':
          return {
            ...prevState,
            userLogin: action.login,
            userPassword: action.password,
          };
        case 'CALL_LOGINSCREEN':
          return {
            ...prevState,
            isLoginScreen: true,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isLoginScreen: false,
            payload: action.payload,
            savedInStorage: true,
            isLoading: false,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            userLogin: '',
            userPassword: '',
            isLoginScreen: true,
            savedInStorage: false,
            isLoading: false,
          };
      }
    },
    {
      isLoading: true,
      payload: '',
      userLogin: '',
      userPassword: '',
      isLoginScreen: true,
      savedInStorage: false,
    }
  );
  
  useEffect(() => {
    console.log('useEffect сработал из App.js');
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      console.log('Пришло сообщение НАВЕРНО при запущенном приложении');
      Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    });
    return unsubscribe;
  }, []);

  useEffect(() => {
    const retrieveData = async () => {
      let entry;
      try {
        const value = await AsyncStorage.getItem('LOGIN_LIMANET');
        if (value !== null || value !== undefined) {
          entry = JSON.parse(value);
          let data = {'username': entry.loginStorage, 'password': entry.passwordStorage};
          authContext.signIn(data);
        }else{
          authContext.signOut();
        }
      } catch (error) {

        authContext.signOut();
      }
    };
    retrieveData();
  }, []);
 
  const authContext = useMemo(
    () => ({
      signIn: async data => {
        fetch('API/validation',{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                login: data.username,
                pass: data.password
            })
        })
        .then((response) => response.json())
        .then((responseJson) => {
          if(responseJson.connect_db === 'ok' && responseJson.result === 'no'){
            Alert.alert('Неверный Логин или Пароль!','Проверьте правильность Логина и Пароля');
            return
          };
          if(responseJson.connect_db !== 'ok'){
            Alert.alert('Ошибка работы базы данных!','Мы устраняем проблему, повторите попытку позже !');
            return
          }
          if(responseJson.connect_db === 'ok' && responseJson.result === 'yes'){
            if (state.savedInStorage === false) {
              let object_logined = {
                  loginStorage: data.username,
                  passwordStorage: data.password,
              };
              AsyncStorage.setItem('LOGIN_LIMANET', JSON.stringify(object_logined), () => {
                dispatch({ type: 'SIGN_IN', payload: responseJson });
              });
            }else{
              dispatch({ type: 'SIGN_IN', payload: responseJson });
            } 
          }
        })
        .catch((error) => {
          console.log(error);
          Alert.alert('Ошибка !','Повторите попытку через некоторое время !');
          dispatch({ type: 'SIGN_OUT' });
        });
      },
      signOut: () => {
        const value = AsyncStorage.getItem('LOGIN_LIMANET');
        if (value !== null || value !== undefined) {
          console.log('УСЛОВИЕ РАБОТАЕТ');
          const removeValue = async () => {
            try {
              await AsyncStorage.removeItem('LOGIN_LIMANET', () => { dispatch({ type: 'SIGN_OUT' }) });
            } catch(e) {
              console.log(e);
            };
          };
          removeValue();
        };
        if (state.savedInStorage === true) {
          
        };
        console.log('Dispatch РАБОТАЕТ');
        dispatch({ type: 'SIGN_OUT' });
      },
      signUp: async data => {
        // In a production app, we need to send user data to server and get a token
        // We will also need to handle errors if sign up failed
        // After getting token, we need to persist the token using `AsyncStorage`
        // In the example, we'll use a dummy token

        dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
      },
    }),
    []
  );

  if(state.isLoading){
    return(
      <LoadingScreen/>
    );
  }
  // console.log(state.payload);
  return (
    <NavigationContainer>
      <StatusBar barStyle="dark-content" backgroundColor="#ffffff"/>
      <AuthContext.Provider value={authContext}>
        <Stack.Navigator
          screenOptions={{
            headerStyle: {
              backgroundColor: '#1e56b1',
            },
            headerTransparent: true,
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: '100',
            },
            headerTitleAlign: 'center',
            gestureEnabled: true,
            ...TransitionPresets.SlideFromRightIOS,
          }}
        >
          {state.isLoginScreen ? (
            <>
              <Stack.Screen name="LoginScreen" options={{ headerShown: false }} component={LoginScreen} />
            </>
          ) : (
            <>
              <Stack.Screen name="Home" component={Home}  options={{ headerTitle: props => <LogoTitle {...props} /> }} initialParams= {state.payload}/>
              <Stack.Screen name="Statistics" component={Statistics} options={{title: 'Моя статистика'}} initialParams= {state.payload}/>
              <Stack.Screen name="Payment" component={Payment} />
              <Stack.Screen name="News" component={News} />
              <Stack.Screen name="Promotion" component={Promotion} options={{title: 'Техподдержка'}} initialParams= {state.payload}/>
              <Stack.Screen name="Tariffs" component={Tariffs} />
              <Stack.Screen name="Settings" component={Settings} />
            </>
          )}
        </Stack.Navigator>
      </AuthContext.Provider>
    </NavigationContainer>
  );
};
